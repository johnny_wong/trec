#!/usr/bin/python2.6

import time
import argparse
import sqlite3
import sys
import os
import time

default_db = "hours.db"
semm_tag = "13s1"
weeks = ['wk01', 'wk02', 'wk03', 'wk04', 'wk05', 'wk06', 'wk07', 'mdsm', 'wk08', 'wk09', 'wk10', 'wk11', 'wk12', 'stvc', 'exm1', 'exm2', 'exm3']

def load_db():
	return sqlite3.connect(default_db)

class init_db(argparse.Action):
	def __call__(self, parser, namespace, values, opt_str):
		conn = load_db()
		with conn:
			cur = conn.cursor()
			cur.execute("CREATE TABLE courses%s (id integer primary key asc autoincrement, name string)" % (semm_tag))
			init_classes = []
			for c in values:
				init_classes.append((None, c))
			cur.executemany("INSERT INTO courses%s VALUES(?, ?)" % (semm_tag), init_classes)

			cur.execute("CREATE TABLE weeks%s (id integer primary key asc autoincrement, name string)" % (semm_tag))
			init_weeks = []
			for week in weeks:
				init_weeks.append((None, week))
			cur.executemany("INSERT INTO weeks%s VALUES(?, ?)" % (semm_tag), init_weeks)

			cur.execute("CREATE TABLE times{0} (course_id integer, date integer, time integer, week_id integer, foreign key(course_id) references courses{0}(id), foreign key(week_id) references weeks{0}(id))".format(semm_tag))
		sys.exit()

def start_timer(conn, course_id, week_id):
	print time.strftime("%H:%M")

	start_time = int(time.time())
	try:
		count = 0
		while True:
			time.sleep(60)
			count += 1
	except KeyboardInterrupt:
		print "%d minutes" % (count, )
		if count > 0:
			with conn:
				cur = conn.cursor()
				cur.execute("INSERT INTO times%s VALUES(%d, %d, %d, %d)" % (semm_tag, course_id, start_time, count, week_id))
			print "added."

def get_id(conn, table, tag):
	with conn:
		cur = conn.cursor()
		cur.execute("SELECT * FROM %ss%s WHERe name = ?" % (table, semm_tag), (tag,))
		result = cur.fetchone()
		if result is None:
			sys.stderr.write("Error: Invalid %s tag '%s'.\n" % (table, tag))
			sys.exit()
		return result[0]

class get_tag_stats(argparse.Action):
	def __call__(self, parser, namespace, values, opt_str):
		conn = load_db()
		col = 'course' if opt_str == '-c' else 'week'
		tag = get_id(conn, col, values)
		with conn:
			cur = conn.cursor()
			cur.execute("SELECT sum(time) FROM times%s WHERe %s_id = ?" % (semm_tag, col), (tag,))
			print cur.fetchone()[0]
		sys.exit()

class start_last_timer(argparse.Action):
	def __call__(self, parser, namespace, values, opt_str):
		conn = load_db()
		with conn:
			cur = conn.cursor()
			cur.execute("SELECT * FROM times%s WHERE date = (SELECT MAX(date) FROM times%s)" % (semm_tag, semm_tag))
			last = cur.fetchone()
			start_timer(conn, last[0], last[3])
		sys.exit()

def args_handler():
	# callbacks that terminate program
	parser = argparse.ArgumentParser()
	parser.add_argument('-v', '--version', action='version', version='%(prog)s v0.1')
	parser.add_argument('-i', help='initialise a new table with courses c1, c2, etc.', nargs='+', metavar=('c1', 'c2'), action=init_db)
	parser.add_argument('-c', '--course', help='get course stats', action=get_tag_stats)
	parser.add_argument('-w', '--week', help='get week stats', action=get_tag_stats)
	parser.add_argument('-l', '--last', nargs=0, help='times with previous class and week', action=start_last_timer)

	# callbacks that return from this function
	parser.add_argument("course", help="class to record")
	parser.add_argument("week", help="week to record")
	return parser.parse_args()

def main():
	args = args_handler()

	conn = load_db()
	with conn:
		cur = conn.cursor()
		course_id = get_id(conn, "course", args.course)
		week_id = get_id(conn, "week", args.week)

		start_timer(conn, course_id, week_id)

if __name__ == "__main__":
	main()
